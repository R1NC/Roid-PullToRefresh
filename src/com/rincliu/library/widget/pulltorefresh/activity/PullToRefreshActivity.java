package com.rincliu.library.widget.pulltorefresh.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.rincliu.library.widget.pulltorefresh.PullToRefreshAbsView.PullToRefreshListener;
import com.rincliu.library.widget.pulltorefresh.PullToRefreshGridView;
import com.rincliu.library.widget.pulltorefresh.PullToRefreshListView;
import com.rincliu.library.widget.pulltorefresh.PullToRefreshScrollView;

public class PullToRefreshActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // final PullToRefreshListView refreshableView = new
        // PullToRefreshListView(this);
        final PullToRefreshGridView refreshableView = new PullToRefreshGridView(this);
        // final PullToRefreshScrollView refreshableView = new
        // PullToRefreshScrollView(this);
        setContentView(refreshableView);
        String[] items = new String[100];
        for (int i = 0; i < 100; i++) {
            items[i] = i + "";
        }
        // ListView contentView = new ListView(this);
        GridView contentView = new GridView(this);
        contentView.setColumnWidth(200);
        contentView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items));
        // ScrollView contentView=new ScrollView(this);
        // TextView tv=new TextView(this);
        // tv.setHeight(1500);
        // contentView.addView(tv);
        refreshableView.setContentView(contentView);
        refreshableView.setOnRefreshListener(new PullToRefreshListener() {
            @Override
            public void onRefresh() {
                // Emulate the process of refreshing
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                refreshableView.finishRefreshing();
            }
        });
    }
}
