/**
 * Copyright (c) 2013-2014, Rinc Liu (http://rincliu.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.rincliu.library.widget.pulltorefresh;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

public class PullToRefreshExpandableListView extends PullToRefreshAbsView {

    /**
     * @param context
     */
    public PullToRefreshExpandableListView(Context context) {
        super(context);
    }

    /**
     * @param context
     * @param attrs
     */
    public PullToRefreshExpandableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private ExpandableListView expandableListView;

    /**
     * @param expandableListView
     */
    public void setContentView(ExpandableListView expandableListView) {
        this.expandableListView = expandableListView;
        super.addContentView();
    }

    @Override
    public ViewGroup onCreateContentView() {
        if (expandableListView == null) {
            throw new NullPointerException("Have you called setContentView?");
        }
        return expandableListView;
    }

    @Override
    public boolean onCheckIsTop() {
        boolean res = false;
        View firstChild = expandableListView.getChildAt(0);
        if (firstChild != null) {
            int firstVisiblePos = expandableListView.getFirstVisiblePosition();
            if (firstVisiblePos == 0 && firstChild.getTop() == 0) {
                res = true;
            }
        }
        return res;
    }
}
